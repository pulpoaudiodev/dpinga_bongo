/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIConfiguration.cpp
//  DryAged
//
//  Created by Rudolf Leitner on 20/02/17.
//
//

#include "UIConfiguration.hpp"
#include "../ProductDef.h"
#include "JuceHeader.h"


UIElementConfig::UIElementConfig()
{
    x=-1;
    y=-1;
    w=-1;
    h=-1;
    sizeX=-1;
    sizeY=-1;
    visible = true;
    showTextBox = false;
}

bool UIElementConfig::hasBounds()
{
    return (x>-1 && y>-1 && w>-1 && h>-1);
}
bool UIElementConfig::hasSize()
{
    return (sizeX>-1 && sizeY>-1);
}






UIConfig::UIConfig()
{
    w = 600;
    h = 480;
    showBackground = false;
    showReloadPatchesButton = false;
    showReloadUIButton = false;
    aboutX = 0;
    aboutY = 0;
    aboutH = 0;
    aboutW = 0;
    
}

UIConfig::~UIConfig()
{
    cleanup();
}



void UIConfig::cleanup()
{
    elementConfigs.clear();
    
    //cleanup the lookandfeel map. delete the lookandfeel objects as they are just pointers
    for(std::map<std::string, juce::LookAndFeel*>::iterator it = lookAndFeels.begin(); it != lookAndFeels.end(); it++)
    {
        delete it->second;
    }
    lookAndFeels.clear();
}


void UIConfig::addElementConfig( UIElementConfig& aElementConfig)
{
    elementConfigs.push_back(aElementConfig);
}

UIElementConfig* UIConfig::getElementConfig( std::string aName)
{
    int n=elementConfigs.size();
    for( int i=0; i<n;i++) {
        UIElementConfig& uec = elementConfigs.at(i);
        if( uec.name == aName) {
            return &uec;
        }
    
    }
    return NULL;
}


juce::LookAndFeel* UIConfig::getLookAndFeel( std::string& aName )
{
    if( lookAndFeels.find( aName ) != lookAndFeels.end() ) {
        return  (lookAndFeels[aName]);
    }
    return nullptr;
}

void UIConfig::addLookAndFeel( std::string& aName, juce::LookAndFeel* aLookAndFeel)
{
    lookAndFeels[aName] = aLookAndFeel;
}

