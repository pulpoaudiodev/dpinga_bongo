/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PatchCreator.hpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 05/11/16.
//
//

#ifndef Configurator_hpp
#define Configurator_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "PatchDefinition.hpp"
#include "UIConfiguration.hpp"
#include "JuceHeader.h"
#include "SampleBin.hpp"


/*--------------------------------------------------------------------------------------------------------------
 * AHDSREnvelopeDefinition
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class Configurator  {
private:
    
    
    void createSoundDefinitionFromXML( SoundDefinition& soundDef, XmlElement* xmlSound );
    void createSoundDefinitionsFromXML( XmlElement* xmlSounds, PatchBank& patchBank );
    void createPatchDefinitionsFromXML( XmlElement* xmlPatches, PatchBank& patchBank );
    
	void createPatchDefinitionFromXML( PatchDefinition& patchDef, XmlElement* xmlPatch, PatchBank& patchBank  );
    UIElementConfig createElementConfigFromXML( XmlElement* xmlUiElement );

public:
	Configurator();

    bool configurePatchBankFromXML(  std::string& aXML, PatchBank& patchBank);
    bool configureUIElementsFromXML( std::string& aXML, SampleBin* aSampleBin, UIConfig& uiConfig );
	
};



#endif /* PatchDefinition_hpp */




